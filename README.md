# DiTauAnalysis

<p align="center">
  <img width="1200" height="800" src="images/eventview.png">
</p>


author: Nicholas Luongo  
contact: nicholas.andrew.luongo@cern.ch

A package of the ATLAS Athena software framework used in the boosted HH->bbtautau analysis. This package, used inside Athena, exposes and calculates the variables to be used for final selection and neural network training. Input files are in the xAOD format and the output are flat ROOT ntuples.

              
Setup
=====


```
git atlas init-workdir https://:@gitlab.cern.ch:8443/atlas/athena.git
cd athena
git clone https://gitlab.cern.ch/nicholas/ditauanalysis.git
mkdir build run
cd build
asetup AthAnalysis,21.2,latest
cmake ../athena/Projects/WorkDir
make
source x86_64-centos7-gcc8-opt/setup.sh
cd ../run
athena DiTau/diTau_jobOptions.py
```
